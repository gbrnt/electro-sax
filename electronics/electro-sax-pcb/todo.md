# Todo
- [ ] Change 47R USB resistors to the correct 27R ones specified in the [hardware design guide](https://datasheets.raspberrypi.com/rp2040/hardware-design-with-rp2040.pdf)
- [ ] Replace BOOTSEL header with a button (not sure why I didn't do this!

# Electro-Sax

Making an electronic saxophone, which can synthesise its own sound into a headphone jack, or for a better sound play the synth of your choice over USB MIDI.

## Current controls

- Buttons in a saxophone-like layout (heavily simplified)
- Breath sensor (detects airflow rate)
- Bite sensor (detects lip pressure)

## Project structure

- `code/`
  - `electro-sax-py/`
    - CircuitPython code for RP2040-based electro-sax v1.0 PCB
  - `python-masking-test/`
    - Testing how to use bit masks to convert combinations of button presses to notes
- `mechanical/`
  - Mechanical design - 3D printed parts, etc
- `electronics/`
  - Electronics design - mkey matrix, etc
  - `electro-sax-pcb/`
    - Main PCB that everything plugs into
  - `electro-sax-wiring/`
    - Internal wiring diagram of the electro-sax
  - `key-pcb/`
    - PCB for conductive silicone keys, with interdigitated contacts and an optional low-pass filter for debouncing

keys = ["Eb", "D", "E",
        "Bb", "C", "F",
        "G#", "G", "A",
        "Oct", "B", "HighD"]

example_presses = [
    ("C#",    0b_000_000_000_000),
    ("HighD", 0b_000_000_000_001),
    ("B",     0b_000_000_000_010),
    ("C#",    0b_000_000_000_100), # Also C# but an octave higher
    ("G",     0b_000_000_011_010),
    ("C#",    0b000_000_000_000),
    ("C",     0b000_000_001_000),
    ("C",     0b000_010_000_010),
    ("B",     0b000_000_000_010),
    ("Bb",    0b000_100_001_010),
    ("A",     0b000_000_001_010),
    ("G#",    0b000_000_111_010),
    ("G",     0b000_000_011_010),
    ("F#",    0b001_000_011_010),
    ("F",     0b000_001_011_010),
    ("E",     0b001_001_011_010),
    ("Eb",    0b111_001_011_010),
    ("D",     0b011_001_011_010),
    ("HighD", 0b000_000_000_001),
]

class Note:
    def __init__(self, name, must_press, must_not_press):
        self.must_press = must_press
        self.must_not_press = must_not_press
        self.name = name

    def booleanInvert(self, num):
        return num ^ 0xFFF

notes = [
    Note("C#",     0b000_000_000_000, 0b000_010_001_011),
    Note("C",      0b000_000_001_000, 0b000_000_000_011),
    Note("C",      0b000_010_000_010, 0b000_000_001_001), # Side key C
    Note("B",      0b000_000_000_010, 0b000_000_001_001),
    Note("Bb",     0b000_100_001_010, 0b000_000_010_001),
    Note("A",      0b000_000_001_010, 0b000_110_010_001),
    Note("G#",     0b000_000_111_010, 0b000_001_000_001),
    Note("G",      0b000_000_011_010, 0b001_001_100_001),
    Note("F#",     0b001_000_011_010, 0b000_001_100_001),
    Note("F",      0b000_001_011_010, 0b001_100_000_001),
    Note("E",      0b001_001_011_010, 0b110_000_000_001),
    Note("Eb",     0b111_001_011_010, 0b000_000_000_001),
    Note("D",      0b011_001_011_010, 0b100_000_000_001),
    Note("HighD",  0b000_000_000_001, 0b000_000_000_000), # High D overrides everything?
    #Note("",      0b000_000_000_000, 0b000_000_000_000),
]


def decodeNote(key_presses):
    # Key presses is an int where we use the 12 LSBs for storing the state of each key

    for note in notes:
        # Check that all "must press" bits are set and all "must not press" ones aren't
        # If this is the case this note is a match!
        if (key_presses & note.must_press == note.must_press) and (key_presses & note.must_not_press == 0):
            return note.name

    return None


if __name__ == "__main__":

    for expected_output, key_presses in example_presses:
        output = decodeNote(key_presses)

        if output == expected_output:
            print("Pass:", output)
        else:
            print(f"Fail: {key_presses:012d} -> {output} (expected {expected_output})")

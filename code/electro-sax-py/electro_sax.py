"""
Electro-sax behind-the-scenes stuff
"""

import board, digitalio, analogio, audiobusio, time, json, storage
import tools, pitch


class Sax:
    def __init__(self):
        self.init_io()
        self.init_matrix()
        self.init_saxophone()

    def init_io(self):
        # Initialise matrix rows and columns
        self.rows = (
            digitalio.DigitalInOut(board.ROW0),
            digitalio.DigitalInOut(board.ROW1),
            digitalio.DigitalInOut(board.ROW2),
            digitalio.DigitalInOut(board.ROW3),
            digitalio.DigitalInOut(board.ROW4),
        )
        for row in self.rows:
            row.pull=digitalio.Pull.DOWN
        self.row_count = len(self.rows)
        self.cols = (
            digitalio.DigitalInOut(board.COL0),
            digitalio.DigitalInOut(board.COL1),
            digitalio.DigitalInOut(board.COL2),
            digitalio.DigitalInOut(board.COL3),
        )
        for col in self.cols:
            col.switch_to_output(value=False)
        self.col_count = len(self.cols)

        # ADCs for analog inputs
        self.adc_0 = analogio.AnalogIn(board.A0)
        self.bite_adc = analogio.AnalogIn(board.ADC_BITE)
        self.breath_adc = analogio.AnalogIn(board.ADC_BREATH)
        self.adc_3 = analogio.AnalogIn(board.A3)

        # LEDs
        self.LED_1 = digitalio.DigitalInOut(board.LED_USER1)
        self.LED_2 = digitalio.DigitalInOut(board.LED_USER2)
        self.LED_1.switch_to_output(value=False)

        # Audio output on first spare GPIO
        #self.audio = audiopwmio.PWMAudioOut(board.SPARE0)

        # Other spare GPIOs
        #self.spare_gpio = (
        #    digitalio.DigitalInOut(board.SPARE1),
        #    digitalio.DigitalInOut(board.SPARE2),
        #    digitalio.DigitalInOut(board.SPARE3),
        #)

        # I2S audio output
        # Need to ground breakout board's SCK pin
        self.audio_sck = digitalio.DigitalInOut(board.SPARE0);
        self.audio_sck.switch_to_output(value=False)
        self.audio = audiobusio.I2SOut(
            bit_clock=board.SPARE1,
            word_select=board.SPARE2,
            data=board.SPARE3
        )

    def init_matrix(self):
        """
        Initialise structure of key matrix
        """
        # Values are the number of the bit in self.key_states that each key corresponds to
        self.KEY_OCT_UP = 19
        self.KEY_OCT_DN = 18
        self.KEY_D_SIDE = 17
        self.KEY_C_SIDE = 16
        self.KEY_Bb_SIDE = 15
        self.KEY_B = 14
        self.KEY_Bb_BIS = 13
        self.KEY_A = 12
        self.KEY_G = 11
        self.KEY_Gs = 10
        self.KEY_F = 9
        self.KEY_E = 8
        self.KEY_D = 7
        self.KEY_Eb = 6
        self.KEY_C_LOW = 5
        self.KEY_FUNCTION = 4
        self.KEY_SPARE2 = 3
        self.KEY_SPARE3 = 2
        self.KEY_SPARE4 = 1
        self.KEY_SPARE5 = 0

        # Matrix is shown the wrong way round here - you access it by self.matrix[column][row]
        self.matrix = (
            (self.KEY_B,      self.KEY_A,      self.KEY_Gs,        self.KEY_E,      self.KEY_Eb),
            (self.KEY_Bb_BIS, self.KEY_G,      self.KEY_F,         self.KEY_D,      self.KEY_C_LOW),
            (self.KEY_D_SIDE, self.KEY_OCT_DN, self.KEY_Bb_SIDE,   self.KEY_SPARE2, self.KEY_SPARE4),
            (self.KEY_OCT_UP, self.KEY_C_SIDE, self.KEY_FUNCTION,  self.KEY_SPARE3, self.KEY_SPARE5)
        )
        self.inverted_keys = (
            (False, False, False, False, False),
            (False, False, False, False, False),
            (False, False, False, False, False),
            (False, False, False, False, False),
        )

    def init_saxophone(self):
        """
        Initialise saxophone-related variables
        """
        # Variables to keep track of which keys are and have been pressed
        self.key_states = 0
        self.last_key_states = 0

        self.midi_note = 0
        self.last_midi_note = 0

        self.breath_min = 0
        self.breath_max = 0
        self.breath_raw = 0  # Raw ADC value
        self.breath = 0      # Normalised to something sensible - 0-1?
        self.last_breath = 0

        self.bite_min = 0
        self.bite_max = 0
        self.bite_raw = 0  # Raw ADC value
        self.bite = 0      # Normalised to something sensible - 0-1?
        self.last_bite = 0

        self.transpose = -9

    def scan_matrix(self):
        """
        Scan the matrix of switches
        Return a single integer with a bit per button in the matrix
        """
        self.key_states = 0

        # Scan through the matrix, setting columns high and then reading the rows
        for c, col in enumerate(self.cols):
            col.value = True
            for r, row in enumerate(self.rows):
                # If this key is pressed, set its bit in the matrix
                # Invert the key if needed
                if not self.inverted_keys[c][r]:
                    self.key_states |= row.value << self.matrix[c][r]
                else:
                    self.key_states |= (not row.value) << self.matrix[c][r]
            col.value = False
            # This sleep allows the capacitance to discharge - maybe it can be reduced?
            time.sleep(0.000001)

        # Rearrange to match logical order of keys instead of matrix order
        return self.key_states

    def key_pressed(self, key=None):
        """
        Check the stored state of a single key in the matrix
        """
        if key is not None:
            return self.key_states & (1 << key)
        else:
            # If key isn't specified just return whether any key is pressed
            return self.key_states != 0

    def key_was_pressed(self, key=None):
        """
        Check the stored state of a single key in the matrix from last time
        """
        if key is not None:
            return self.last_key_states & (1 << key)
        else:
            # If key isn't specified just return whether any key was pressed
            return self.last_key_states != 0

    def read_single_key(self, key):
        """
        Read the state of a specific key from the matrix
        """
        # Scan through the matrix to find the right key (no easy reverse way right now)
        for c, col in enumerate(self.cols):
            for r, row in enumerate(self.rows):
                # Once we find the right key, scan it then exit early
                if self.matrix[c][r] == key:
                    col.value = True
                    if not self.inverted_keys[c][r]:
                        key_state = row.value
                    else:
                        key_state = not row.value
                    col.value = False
                    return key_state

    def update_last_values(self):
        """
        Update versions of variables for tracking the last cycle
        """
        self.last_key_states = self.key_states
        self.last_midi_note = self.midi_note
        self.last_breath = self.breath
        self.last_bite = self.bite

    def keys_to_note(self, keys=None):
        """
        Convert state of pressed keys to a MIDI state
        """
        if keys is None:
            keys = self.key_states

        note = None

        for combo in note_combos:
            # If the keys match the required pressed keys and don't match the required unpressed keys,
            # that's the note! We can stop looking
            if keys & combo.pressed == combo.pressed and not keys & combo.not_pressed:
                note = combo.midi_note
                break

        # If we didn't find a note that matches the pressed keys, set it to 0
        # We'll have to handle that in the function that actually plays the note
        if note is None:
            self.midi_note = 0
            return 0
        else:
            # Go up or down an octave if relevant octave key is pressed
            if self.key_pressed(self.KEY_OCT_UP):
                note += 12
            elif self.key_pressed(self.KEY_OCT_DN):
                note -= 12

            note += self.transpose

            self.midi_note = note
            return self.midi_note + self.transpose

    def adjust_transpose(self):
        """
        Use user input to adjust the key the electro-sax plays in
        """
        pass

    def read_breath_sensor(self):
        """
        Store and return 7-bit value for breath-sensor pin
        """
        self.breath_raw = self.breath_adc.value
        self.breath = tools.map_range(self.breath_raw, self.breath_min, self.breath_max, 0, 1)
        return self.breath

    def read_bite_sensor(self):
        """
        Store and return 7-bit value for bite-sensor pin
        """
        self.bite_raw = self.bite_adc.value
        self.bite = tools.map_range(self.bite_raw, self.bite_min, self.bite_max, 0, 1)
        return self.bite

    def calibrate_analog_sensor(self, adc, msg_min, msg_max):
        """
        Set minimum and maximum values for breath sensor
        """
        # Temp values for now until calibration is implemented
        readings_to_average = 16
        reading_min = 0
        reading_max = 0
        print(msg_min)
        time.sleep(0.5)  # Sleep instead of properly debouncing the button

        # Wait for octave+ button to be pressed
        while not self.read_single_key(self.KEY_OCT_UP):
            time.sleep(0.001)

        # Average several readings to reduce noise
        # (hopefully this actually triggers it to take multiple readings)
        for i in range(readings_to_average):
            reading_min += adc.value
        reading_min /= readings_to_average
        print(f"Min: {reading_min}")

        time.sleep(0.5)  # Sleep instead of properly debouncing the button again

        print(msg_max)

        # Wait for octave+ button to be pressed
        while not self.read_single_key(self.KEY_OCT_UP):
            time.sleep(0.001)

        for i in range(readings_to_average):
            reading_max += adc.value
        reading_max /= readings_to_average
        print(f"Max: {reading_max}")

        time.sleep(0.5)
        return reading_min, reading_max

    def calibrate_breath_sensor(self):
        """
        Set minimum and maximum values for breath sensor
        """
        self.breath_min, self.breath_max = self.calibrate_analog_sensor(
            self.breath_adc, "Calibrating breath - blow at minimum and press Octave+",
            "Blow at maximum and press Octave+")
        self.write_calibration_to_storage()

    def calibrate_bite_sensor(self):
        """
        Set minimum and maximum values for bite sensor
        """
        self.bite_min, self.bite_max = self.calibrate_analog_sensor(
            self.bite_adc, "Calibrating bite - bite at minimum and press Octave+",
            "Bite at maximum and press Octave+")
        self.write_calibration_to_storage()

    def write_calibration_to_storage(self):
        """
        Write persistent calibration values to storage
        Stored in a file called calibration.json
        """
        calibration = {
            "breath_min": self.breath_min,
            "breath_max": self.breath_max,
            "bite_min": self.bite_min,
            "bite_max": self.bite_max
        }
        try:
            with open("calibration.json", "w+") as cal_file:
                json.dump(calibration, cal_file)
            print("Wrote calibration to storage")
        except OSError as e:
            print("Could not write calibration to storage - not writeable")


    def load_calibration_from_storage(self):
        """
        Load persistent calibration values from storage
        Stored in a file called calibration.json
        """
        with open("calibration.json", "r") as cal_file:
            calibration = json.load(cal_file)

        self.breath_min = calibration["breath_min"]
        self.breath_max = calibration["breath_max"]
        self.bite_min = calibration["bite_min"]
        self.bite_max = calibration["bite_max"]
        print("Loaded calibration from EEPROM")


class NoteCombo:
    def __init__(self, midi_note: int, pressed: int, not_pressed: int):
        self.midi_note = midi_note
        self.pressed = pressed  # Keys which must be pressed
        self.not_pressed = not_pressed  # Keys which must not be pressed

note_combos = [
    NoteCombo(pitch.D5b, 0b00000000000000000000, 0b00110101000000000000),  # No keys pressed
    NoteCombo(pitch.C5,  0b00000001000000000000, 0b00100100000000000000),  # Normal C using A key
    NoteCombo(pitch.C5,  0b00010100000000000000, 0b00100000000000000000),  # Side key C
    NoteCombo(pitch.B4,  0b00000100000000000000, 0b00110011000000000000),  # Normal B
    NoteCombo(pitch.B4b, 0b00000110000000000000, 0b00110001000000000000),  # Bb using key between B and A
    NoteCombo(pitch.B4b, 0b00001101000000000000, 0b00100000100000000000),  # Normal Bb using side Bb key
    NoteCombo(pitch.A4,  0b00000101000000000000, 0b00111000100000000000),  # Normal A
    NoteCombo(pitch.A4b, 0b00000101110000000000, 0b00100000001000000000),  # Normal G#
    NoteCombo(pitch.G4,  0b00000101100000000000, 0b00100000011100000000),  # Normal G
    NoteCombo(pitch.G4b, 0b00000101100100000000, 0b00100000011000000000),  # Normal F#
    NoteCombo(pitch.F4,  0b00000101101000000000, 0b00101000000100000000),  # Normal F
    NoteCombo(pitch.E4,  0b00000101101100000000, 0b00100000000010000000),  # Normal E
    NoteCombo(pitch.E4b, 0b00000101101111000000, 0b00100000000000000000),  # Normal Eb
    NoteCombo(pitch.D4,  0b00000101101110000000, 0b00100000000001000000),  # Normal D
    NoteCombo(pitch.G5b, 0b00100000000100000000, 0b00000000001000000000),  # High F# using side D and right hand key for F#
    NoteCombo(pitch.F5,  0b00100000001000000000, 0b00000000000100000000),  # High F using side D and right hand keys for F
    NoteCombo(pitch.E5,  0b00100000001100000000, 0b00000000000010000000),  # High E using side D and right hand keys for E
    NoteCombo(pitch.E5b, 0b00100000001111000000, 0b00000000000000000000),  # High Eb using side D and right hand keys for Eb
    NoteCombo(pitch.D5,  0b00100000001110000000, 0b00000000000001000000),  # High D using side D and right hand keys for D
    NoteCombo(pitch.D5,  0b00100000000000000000, 0b00000000001100000000)   # Side key D
]

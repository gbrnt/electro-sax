"""
Sound-related variables and functions for electro-sax
"""

import synthio
import ulab.numpy as np

# Create single-cycle sawtooth waveform
SAMPLE_SIZE = 512
SAMPLE_VOLUME=32000 # 0-32767
wave_saw = np.linspace(SAMPLE_VOLUME, -SAMPLE_VOLUME, num=SAMPLE_SIZE, dtype=np.int16)

# Initialise synthesizer
synth = synthio.Synthesizer(sample_rate=22050, waveform=wave_saw)
note_envelope = synthio.Envelope(
        attack_time=0.01,
        attack_level=1.0,
        decay_time=0.05,
        sustain_level=1.0,
        release_time=0.1)
synth.envelope = note_envelope

# Low frequency filter for playing softly, higher for playing loudly
# Interpolate between them as breath pressure changes to make sound change
filter_low = synth.low_pass_filter(frequency=1024)
filter_high = synth.low_pass_filter(frequency=8192)
filter_low_coefficients = (filter_low.a1, filter_low.a2, filter_low.b0, filter_low.b1, filter_low.b2)
filter_high_coefficients = (filter_high.a1, filter_high.a2, filter_high.b0, filter_high.b1, filter_high.b2)

def interpolate_filter(fraction):
    """
    Create a filter partway between filter_low and filter_high
    Distance determined by fraction from 0 to 1
    """
    coeffs = tuple(l*(1-fraction)+h*fraction for l, h in zip(filter_low_coefficients, filter_high_coefficients))
    return synthio.Biquad(a1=coeffs[0], a2=coeffs[1], b0=coeffs[2], b1=coeffs[3], b2=coeffs[4])

# The one note we'll be playing since this is a mono synth
note = synthio.Note(frequency=synthio.midi_to_hz(56), amplitude=0.0, waveform=wave_saw,
                    envelope=note_envelope, filter=interpolate_filter(0))

# Mirror synthio's midi_to_hz function so synthio doesn't need to be directly included in main file
midi_to_hz = synthio.midi_to_hz

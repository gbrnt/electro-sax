"""
Electro-sax main program
"""

import time, tools, synthio
import electro_sax
import sound

print("Starting...")

# Sax object stores the stuff related to the actual hardware
sax = electro_sax.Sax()
sax.audio.play(sound.synth)
sound.synth.press(sound.note)

# Temp calibration of analog sensors
sax.load_calibration_from_storage()

while True:
    sax.scan_matrix()
    sax.keys_to_note()
    sax.read_breath_sensor()
    sax.read_bite_sensor()

    if sax.breath != sax.last_breath:
        sound.note.amplitude = sax.breath
        sound.note.filter = sound.interpolate_filter(sax.breath)

    if sax.midi_note != sax.last_midi_note:
        sound.note.frequency = sound.midi_to_hz(sax.midi_note)

    if sax.key_pressed(sax.KEY_FUNCTION):
        # Function key has been pressed - let's calibrate! First we need to figure out what to calibrate
        if sax.key_pressed(sax.KEY_D_SIDE):
            sax.calibrate_breath_sensor()
        elif sax.key_pressed(sax.KEY_Bb_SIDE):
            sax.adjust_transpose()
        elif sax.key_pressed(sax.KEY_C_SIDE):
            sax.calibrate_bite_sensor()

    # If biting adjust pitch - up 1 semitone for now
    #if sax.bite != 0:
    #    current_frequency = synthio.midi_to_hz(sax.midi_note)
    #    next_semitone_frequency = synthio.midi_to_hz(sax.midi_note + 1)
    #    note1.frequency = current_frequency*(1-sax.bite) + next_semitone_frequency*sax.bite


    sax.update_last_values()

    time.sleep(0.01)

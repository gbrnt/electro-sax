"""
Boot.py - if Function key is pressed at startup, allow computer to write to it
This means the sax can only be programmed by plugging it in or resetting while holding Function
"""

import board, digitalio, storage, time

#storage.remount("/", readonly=False)
col = digitalio.DigitalInOut(board.COL3)
col.switch_to_output(value=True)
row = digitalio.DigitalInOut(board.ROW2)
row.pull = digitalio.Pull.DOWN

time.sleep(0.1)  # Allow any capacitance to charge/discharge

# Check if Function key is pressed
# If high it's pressed and we should enable the board to be programmed
# If low it's not pressed and we should allow circuitpython to access the board
if row.value:
    storage.remount("/", readonly=True)
    print("Initialised storage computer-readable - sax can be programmed")
else:
    storage.remount("/", readonly=False)
    print("Initialised storage board-readable - sax cannot be programmed")

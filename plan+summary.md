# Electro-Sax Plan and Summary
## Version 1
- Initial test to see if it felt any good
- Cardboard body (triangular prism)
- Gateron Brown switches with round "typewriter" style caps
- Cardboard + bottle top + balloon breath sensor using LED + LDR
- Arduino Pro Micro
- Buttons felt good but activation point was too early
- Breath sensor wasn't sensitive enough but proved concept worked!

## Version 2
- Cardboard body (cuboid)
- Flat capacitive buttons
- 3D printed adjustable height mouthpiece based around TCRT5000
- Teensy LC (for easy capacitive sensing)
- Was very nice and responsive, but felt more like a recorder than a saxophone (because no key movement)
- Breath sensor was a huge improvement, and adjustability allowed me to find ideal distance for sensor from membrane

## Version 3
- Cardboard body
- 3D printed Microswitch-based switches
- Didn't put a breath sensor on because the buttons were far too clicky to use!

## Version 4
- Cardboard body with 3D printed plates added around keys
- 3D printed mechanical buttons with capacitive sensing
- 3D printed fancier-looking breath sensor
- Teensy LC (for easy capacitive sensing)
- I used this for a long time because the lever-style switches felt really good!
- Breath sensor was good too but eventually it (and/or the Teensy's USB connector) started to fail.
- The thinner mouthpiece was a big improvement!

## Version 5
### Aims

- Improve consistency/reliability of capacitive sensing
  - Not require manual calibration every startup!
- Improve reliability of breath sensor
  - Rubber longevity
  - Light tightness
  - Condensation drainage
  - Not require manual calibration every startup!
- Improve build quality (no cardboard!)
  - Make easier to disassemble (no gaffer tape!)
  - Sturdier - more consistent positioning
- Keep nice mechanical feel, or make it more like a real sax
  - Different button travels for front keys, side keys and back keys
  - Higher resistance against pressing side keys and back keys
- Improve electronics
  - PCBs? May be too early
  - Stronger USB connector (USB C or full-size USB B)

### R&D Areas

- Keys
  - Switch instead of capacitive button
    - Similar to membrane switch, using conductive silicone
  - Hide copper surfaces
    - Conductive filament for keys?
  - Minifying external part
- Breath sensor
  - Alternative membrane (balloon deteriorates? May have just been UV)
  - Making breath sensor smaller
    - TCRT5000's min 15mm distance makes it tricky
    - Use discrete IR LED + phototransistor
  - Use pressure sensor chip?
    - This is the "right" solution but they're a little hard to find and quite expensive at the moment!
  - Better drainage
    - Tube out of bottom?
- Mouthpiece
  - Make it more saxophone like
    - Fake reed?
    - Opening on bottom side but still near very end
  - Add bite sensor
  - Make it easier to clean
    - Removable? Needs seals
    - Not 3D printed? Cast for cleaner geometry?
